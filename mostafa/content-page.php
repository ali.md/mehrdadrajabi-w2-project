<div class="container-content">
	
	<div class="content">
        <?php
			get_sidebar('left');

		?>
		<div class="last content-img">
	        <?php
		        if($post->post_name == 'home'){
				get_template_part('content', 'img');
				}else{

				if(have_posts()){
					while(have_posts()){
						the_post();
			?>
					<article class='post'>
						<h2 class="about-header"><?php the_title(); ?></h2>
                        <?php
	                        if($post->post_name == 'contact-me'){
	                        get_template_part('content', 'contact');
	                        }else{
						?>
						<p>
							<?php the_content(); ?>
						</p>
					</article>
			<?php
							}}
				}else{
					echo 'Nothing ...';
				}
			}
	        ?>
        </div>
		<div class="clear"></div>
	</div>				

</div>