<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<title>Photoblog</title>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/1styles.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/style.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/lightbox.css"  media="screen" />
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/sliding_effect.js"></script> 
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/javascript.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/jquery-ui.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/lightbox.js"></script>
</head>
<body>
<div class="container-header">
	<div class="header">
		<div class="logo left w5"><a href="#">Photoblog</a></div>
        <div class="menu ml5 last">
         	<ul>
            	<?php
	            	wp_list_pages(array(
				  		'title_li' => '',
						'child_of' => 9,
						'depth'    => 1,
						'id'  	   => 'mos'
	                ));
				?>
         	</ul>        
        </div>
        <div class="clear"></div>
	</div>
</div>